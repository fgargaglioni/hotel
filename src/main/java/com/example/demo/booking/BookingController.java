package com.example.demo.booking;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/bookings")
public class BookingController {

    private final BookingService bookingService;

    private final BookingServiceDto bookingServiceDto;

    @Autowired
    public BookingController(BookingService bookingService, BookingServiceDto bookingServiceDto) {
        this.bookingService = bookingService;
        this.bookingServiceDto = bookingServiceDto;
    }


    @GetMapping
    public List<Booking> getBookings(){return bookingService.getBookings();}

    @PostMapping
    public void registerBooking(@RequestBody BookingDto bookingDto){ bookingService.registerBooking(bookingServiceDto.bookingADto(bookingDto));}

    @PutMapping(path = "cancel/{bookingId}")
    public void cancelBooking(@PathVariable ("bookingId") Integer bookingId) throws Exception {
        bookingService.cancel(bookingId);}
}