package com.example.demo.booking;

import com.example.demo.customer.Customer;
import com.example.demo.room.Room;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private LocalDate checkIn;
    private LocalDate checkOut;

    @ManyToOne
    @JoinColumn(name = "costumer_id")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;
    private boolean canceled;


    public Booking(){}

//    public Booking(Integer id, LocalDate checkIn, LocalDate checkOut, Customer customer, Room room) {
//        this.id = id;
//        this.checkIn = checkIn;
//        this.checkOut = checkOut;
//        this.customer = customer;
//        this.room = room;
//        this.canceled = canceled;
//    }

    public Booking(Room room, Customer customer, LocalDate checkIn, LocalDate checkOut) {
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.customer = customer;
        this.room = room;
        this.canceled = false;
    }

//    public double getPrice() {
//        return room.calcPrice(daysCount());
//    }

//    public int daysCount() {
//        return (int) checkIn.datesUntil(checkOut.plusDays(1)).count();
//    }


//    public Room getRoom() {
//        return room;
//    }


    //SETTERS//

    public void setId(Integer id){this.id = id;}

    public void setRoom(Room room){this.room = room;}

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }


    //GETTERS//

    public Room getRoom(){return this.room;}

    public Integer getId() {
        return this.id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", checkIn=" + checkIn +
                ", checkOut=" + checkOut +
                ", customer=" + customer +
                ", room=" + room +
                ", canceled=" + canceled +
                '}';
    }
}
