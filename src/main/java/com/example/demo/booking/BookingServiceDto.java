package com.example.demo.booking;

import com.example.demo.customer.Customer;
import com.example.demo.customer.CustomerRepository;
import com.example.demo.room.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.print.Book;

@Service
public class BookingServiceDto {

    private final RoomRepository roomRepository;

    private final CustomerRepository customerRepository;

    @Autowired
    public BookingServiceDto(RoomRepository roomRepository, CustomerRepository customerRepository) {
        this.roomRepository = roomRepository;
        this.customerRepository = customerRepository;
    }

    public Booking bookingADto(BookingDto dto){
        Booking b = new Booking(roomRepository.findById(dto.getRoomId()).get(), customerRepository.findById(dto.getCustomerId()).get(),
                dto.getCheckIn(), dto.getCheckOut());
        return b;
    }
}
