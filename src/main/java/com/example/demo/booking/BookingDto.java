package com.example.demo.booking;


import com.example.demo.customer.Customer;
import com.example.demo.room.Room;
import java.time.LocalDate;

public class BookingDto {
    private LocalDate checkIn;
    private LocalDate checkOut;
    private String customerId;
    private Integer roomId;
    private Boolean canceled;

    public BookingDto() {
    }

    public BookingDto(LocalDate checkIn, LocalDate checkOut, String customerId, Integer roomId) {
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.customerId = customerId;
        this.roomId = roomId;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }
}
