package com.example.demo.booking;

import com.example.demo.room.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {


    List<Booking> findBycheckInBetweenAndRoom(LocalDate checkIn, LocalDate checkOut, Room room);

}
