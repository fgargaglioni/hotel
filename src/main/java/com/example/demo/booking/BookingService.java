package com.example.demo.booking;


import com.example.demo.customer.CustomerRepository;
import com.example.demo.room.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
public class BookingService {

    private final CustomerRepository customerRepository;

    private final RoomRepository roomRepository;

    private final BookingRepository bookingRepository;

    @Autowired
    public BookingService(CustomerRepository customerRepository, RoomRepository roomRepository, BookingRepository bookingRepository){
        this.customerRepository = customerRepository;
        this.roomRepository = roomRepository;
        this.bookingRepository = bookingRepository;}

    public List<Booking> getBookings(){return bookingRepository.findAll();}

    public void registerBooking(Booking booking){
        List<Booking> sameRoomBooking = bookingRepository.findBycheckInBetweenAndRoom(booking.getCheckIn(),booking.getCheckOut(),booking.getRoom());
        if (!sameRoomBooking.isEmpty()){
            throw new IllegalStateException("There is already an existing booking in that date");
        }
        bookingRepository.save(booking);
    }

    @Transactional
    public void cancel(Integer bookingId) throws Exception {
            LocalDate now = LocalDate.now();
            Booking b = bookingRepository.findById(bookingId).get();

           if (b.getCheckIn().isAfter(now))
                b.setCanceled(true);
            else
                throw new Exception("Can't cancel a booking that's already in progress");
        }

}
