package com.example.demo.room;

import com.example.demo.booking.Booking;
import org.hibernate.annotations.DiscriminatorFormula;

import javax.persistence.*;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo", discriminatorType = DiscriminatorType.STRING)
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer Id;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Booking> bookings;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<Amenity> amenities;

    private int number;
    private double price;

//    public Room(Integer id, Amenity amenities, int number, double price) {
//        Id = id;
//        this.amenities = amenities;
//        this.number = number;
//        this.price = price;
//    }

    public Room() {
    }

    public Room(List<Booking> bookings, List<Amenity> amenities, int number, double price) {
        this.bookings = bookings;
        this.amenities = amenities;
        this.number = number;
        this.price = price;
    }

    public Room(Integer id, List<Booking> bookings, List<Amenity> amenities, int number, double price) {
        Id = id;
        this.bookings = bookings;
        this.amenities = amenities;
        this.number = number;
        this.price = price;
    }

    public Room( List<Amenity> amenities, int number, double price) {
        this.amenities = amenities;
        this.number = number;
        this.price = price;
    }

    public Integer getId() {
        return this.Id;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Amenity> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Amenity> amenities) {
        this.amenities = amenities;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }


//    public double calcPrice(int daysCount) {
//        double currPrince = (price * daysCount);
//
//        if (daysCount > 7)
//            currPrince -= currPrince * 0.05;
//
//        return currPrince;
//    }

    public String toString() {
        return ("Room Id°: " + number + " Price p/day: " + price);
    }

    public enum Amenity {
        SAFE_BOX,
        MINI_BAR,
        WIFI
    }
}
