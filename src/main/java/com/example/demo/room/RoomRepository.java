package com.example.demo.room;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    @Query
            (value = "SELECT * " +
            "FROM room u " +
            "WHERE u.number =?1",
    nativeQuery = true)
    Optional<Room> findByNumber(int number);

}

