package com.example.demo.room;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/rooms")
public class RoomController {

    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }


    @GetMapping(path = "roomList")
    public List<Room> getRooms(){return roomService.getRooms();}

    @PostMapping
    public void addRoom(@RequestBody Room room){ roomService.addRoom(room);}

    @GetMapping(path = "availability/{roomId}")
    public void checkAvailability(@PathVariable ("roomId") Integer roomId,
                                  @RequestParam String checkIn,
                                  @RequestParam String checkOut){

        LocalDate checkInL = LocalDate.parse(checkIn);

        LocalDate checkOutL = LocalDate.parse(checkOut);

        roomService.checkAvailability(roomId, checkInL, checkOutL);
    }

}