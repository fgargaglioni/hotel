package com.example.demo.room;

import com.example.demo.booking.Booking;

import javax.persistence.*;
import java.util.List;

@Entity
@DiscriminatorValue("LuxuryRoom")
public class LuxuryRoom extends Room {

    public LuxuryRoom() {
    }

    public LuxuryRoom(List<Booking> bookings, List<Amenity> amenities, int number, double price) {
        super(bookings, amenities, number, price);
    }

    public LuxuryRoom(Integer id, List<Booking> bookings, List<Amenity> amenities, int number, double price) {
        super(id, bookings, amenities, number, price);
    }

    public LuxuryRoom(int number, List<Amenity> amenities, double price) {
        super(amenities, number, price);
    }
}
