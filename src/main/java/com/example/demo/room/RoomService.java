package com.example.demo.room;

import com.example.demo.booking.Booking;
import com.example.demo.booking.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final BookingRepository bookingRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository, BookingRepository bookingRepository){this.roomRepository = roomRepository;
        this.bookingRepository = bookingRepository;
    }

    public List<Room> getRooms(){return roomRepository.findAll();}

    public void addRoom(Room room){
        Optional<Room> sameNameRoom = roomRepository.findByNumber(room.getNumber());
        if(sameNameRoom.isPresent()){
            throw new IllegalStateException("There is already a room with that number");
        }
        roomRepository.save(room);
    }

    public void checkAvailability(Integer id, LocalDate checkIn, LocalDate checkOut) {
        Room r = roomRepository.findById(id).get();
        List<Booking> sameDateBookings = bookingRepository.findBycheckInBetweenAndRoom(checkIn, checkOut, r);
        if(!sameDateBookings.isEmpty()){
            throw new IllegalStateException("There is an existing booking in room " + r.getNumber() + " on that date.");
        }

        throw new IllegalStateException("Booking is available.");
    }

}

