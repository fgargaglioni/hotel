package com.example.demo.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,String> {

    @Query(value = "SELECT s " +
            "FROM customers s" +
            " WHERE s.tel_Number = ?1",
    nativeQuery = true)
    Optional<Customer> findCustomerByTelNumber(String telNumber);
}