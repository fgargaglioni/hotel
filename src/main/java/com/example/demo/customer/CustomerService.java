package com.example.demo.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    public List<Customer> getCustomers(){
        return customerRepository.findAll();
    }

    public void addNewCustomer(Customer customer) {
        Optional<Customer> customerByTelNumber = customerRepository.findCustomerByTelNumber(customer.getTelNumber());
        if (customerByTelNumber.isPresent()){
            throw new IllegalStateException("telephone number allready exists");
        }
        customerRepository.save(customer);
        System.out.println(customer);
    }

    public void deleteCostumer(String customerId) {
        boolean exists = customerRepository.existsById(customerId);
        if(!exists){
            throw new IllegalStateException("Customer with" + customerId + " does not exist.");
        }
        customerRepository.deleteById(customerId);
    }
}