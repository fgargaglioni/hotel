package com.example.demo.customer;

import com.example.demo.booking.Booking;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    private String id;
    private String name;
    private String telNumber;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;

    public Customer(){}

    public Customer(String id, String name, String telNumber, List<Booking> bookings) {
        this.id = id;
        this.name = name;
        this.telNumber = telNumber;
        this.bookings = bookings;
    }

    public Customer(String name, String telNumber) {
        this.name = name;
        this.telNumber = telNumber;
    }

    public Customer(String id, String name, String telNumber) {
        this.id = id;
        this.name = name;
        this.telNumber = telNumber;
    }

    public String toString() {
        return ("Name: " + name + " Tel: " + telNumber);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}