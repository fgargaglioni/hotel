package com.example.demo.customer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class CustomerConfig {

    @Bean
    public CommandLineRunner CustomerConfig(CustomerRepository customerRepository) {


        return args -> {
            Customer pablito = new Customer("8998172983", "Pablito", "telPablito");
            Customer juanita = new Customer("8786876", "Juanita", "telJuanita");
            customerRepository.saveAll(List.of(pablito, juanita));
        };
    }
}