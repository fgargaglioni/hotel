package com.example.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, WebMvcAutoConfiguration.class})
@SpringBootApplication
public class ProyectoEndavaSpringApplication {

	private static final Logger LogJava = LogManager.getLogger("fileLogger");

	public static void main(String[] args) {
		LogJava.debug("Debug Message Logged !!!");
		LogJava.info("Info Message Logged !!!");
		SpringApplication.run(ProyectoEndavaSpringApplication.class, args);
	}

}
