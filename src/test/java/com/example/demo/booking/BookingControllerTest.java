package com.example.demo.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BookingControllerTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void getBookings() {
    }

    @Test
    void registerBooking() {
    }

    @Test
    void cancelBooking() {
    }
}